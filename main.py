#!/usr/bin/python3
 
import dbus
from subprocess import check_output
import argparse
import time
import os
from GATT_utils_py.advertisement import Advertisement
from GATT_utils_py.service import Application, Service, Characteristic, Descriptor


MAX_NUM_TRIES = 2 
GATT_CHRC_IFACE = "org.bluez.GattCharacteristic1"
 
# Advertisement for module
class IPAddressAdvertisement(Advertisement):
    def __init__(self, index, name):
        Advertisement.__init__(self, index, "peripheral")
        self.add_local_name(name)
        self.include_tx_power = True
 
# Main service
class IPAddressService(Service):
    IPADDR_SVC_UUID = "00000000-cffb-4231-ad87-db62e82a8017"
 
    def __init__(self, index):
        Service.__init__(self, index, self.IPADDR_SVC_UUID, True)
        self.add_characteristic(IPAddressCharacteristic(self))
        self.add_characteristic(WiFiSSIDCharacteristic(self))
        self.add_characteristic(WiFiPWCharacteristic(self))
        self.WiFiSSID = "none"
        self.WiFiPW = "none"
    
    def connect_to_Network(self):
        print("connecting to network " + self.WiFiSSID + " with password " + self.WiFiPW)
        try:
            connect(self.WiFiSSID, self.WiFiPW)
            return True
        except:
            print("failure!")
            return False
 
# IP characteristic field, return none if no wifi conection
class IPAddressCharacteristic(Characteristic):
    IPADDR_CHARACTERISTIC_UUID = "00000001-cffb-4231-ad87-db62e82a8017"
 
    def __init__(self, service):
        Characteristic.__init__(self, self.IPADDR_CHARACTERISTIC_UUID, ["read"], service)
        self.add_descriptor(IPAddressDescriptor(self))
    def ReadValue(self, options):
        value = []
        
        try:
            ipaddrs = check_output(["hostname", "-I"])
            ipaddr = ipaddrs.split()[0]
        except:
            ipaddr = "none"

        for c in ipaddr:
            value.append(dbus.Byte(c))
 
        return value

# Descriptor for IP characteristic field
class IPAddressDescriptor(Descriptor):
    UNIT_DESCRIPTOR_UUID = "00000002-cffb-4231-ad87-db62e82a8017"
    UNIT_DESCRIPTOR_VALUE = "IP address"

    def __init__(self, characteristic):
        Descriptor.__init__(
                self, self.UNIT_DESCRIPTOR_UUID,
                ["read"],
                characteristic)

    def ReadValue(self, options):
        value = []
        desc = self.UNIT_DESCRIPTOR_VALUE
        for c in desc:
            value.append(dbus.Byte(c.encode()))
        return value
    
# WiFi SSID characteristic field, read and write
class WiFiSSIDCharacteristic(Characteristic):
    WIFI_CHARACTERISTIC_UUID = "00000003-cffb-4231-ad87-db62e82a8017"

    def __init__(self, service):
        Characteristic.__init__(
                self, self.WIFI_CHARACTERISTIC_UUID,
                ["read", "write"], service)
        self.add_descriptor(WiFiSSIDDescriptor(self))

    def WriteValue(self, value, options):
        self.service.WiFiSSID = ''.join([str(v) for v in value])
        print("Set SSID to: " + self.service.WiFiSSID)

    def ReadValue(self, options):
        value = []
        try:
            scan_res = str(check_output(["sudo", "iwgetid"]))
            start = scan_res.find("\"") + 1
            end = scan_res.find("\"", start)
            desc = scan_res[start:end]
        except:
            desc = "none"
        for c in desc:
            value.append(dbus.Byte(c.encode()))
        return value
    
# Descriptor for WiFi SSID characteristic field
class WiFiSSIDDescriptor(Descriptor):
    UNIT_DESCRIPTOR_UUID = "00000004-cffb-4231-ad87-db62e82a8017"
    UNIT_DESCRIPTOR_VALUE = "WiFi SSID"

    def __init__(self, characteristic):
        Descriptor.__init__(
                self, self.UNIT_DESCRIPTOR_UUID,
                ["read"],
                characteristic)

    def ReadValue(self, options):
        value = []
        desc = self.UNIT_DESCRIPTOR_VALUE
        for c in desc:
            value.append(dbus.Byte(c.encode()))
        return value

# WiFi password characteristic field, write only
class WiFiPWCharacteristic(Characteristic):
    WIFI_CHARACTERISTIC_UUID = "00000005-cffb-4231-ad87-db62e82a8017"

    def __init__(self, service):
        Characteristic.__init__(
                self, self.WIFI_CHARACTERISTIC_UUID,
                ["write"], service)
        self.add_descriptor(WiFiPWDescriptor(self))

    def WriteValue(self, value, options):
        self.service.WiFiPW= ''.join([str(v) for v in value])
        print("Set PW to: " + self.service.WiFiPW)
        self.service.connect_to_Network()
    
# Descriptor for WiFi PW characteristic field
class WiFiPWDescriptor(Descriptor):
    UNIT_DESCRIPTOR_UUID = "00000006-cffb-4231-ad87-db62e82a8017"
    UNIT_DESCRIPTOR_VALUE = "WiFi PW"

    def __init__(self, characteristic):
        Descriptor.__init__(self, self.UNIT_DESCRIPTOR_UUID, ["read"], characteristic)

    def ReadValue(self, options):
        value = []
        desc = self.UNIT_DESCRIPTOR_VALUE
        for c in desc:
            value.append(dbus.Byte(c.encode()))
        return value
    
# Connect to wifi network
def connect(goal_ssid,goal_pw):
    os.popen("sudo nmcli d wifi list")
    cm = "sudo nmcli d wifi connect \"" + goal_ssid + "\" password \"" + goal_pw + "\" ifname wlan0 hidden yes"
    for __ in range(0, MAX_NUM_TRIES - 1):
        os.popen(cm)
        time.sleep(1)
    return os.popen(cm)



# Get name from argument
parser = argparse.ArgumentParser(description='Set bluetooth name')
#parser.add_argument('-name', default="Schlusslicht", required=False)
parser.add_argument('-name', default="BT2WIFI_DEVICE", required=False)
args = parser.parse_args()
print("Setting bluetooth name to " + args.name)

# Register service and advertisement
app = Application()
app.add_service(IPAddressService(0))
app.register()
adv = IPAddressAdvertisement(0, args.name)
adv.register()
 
# run until stopped
try:
    app.run()
except KeyboardInterrupt:
    app.quit()
