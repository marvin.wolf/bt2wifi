# BT2WiFi

This module allows to set WiFi SSID and PW via bluetooth low energy (BLE). 
Network SSID and the device's IP are also readable. 

## Reading & writing information

Information (IP address, WiFi SSID, WiFi password) are saved within "characteristics", that are part of the main service:

|Service| UUID| Interaction |
| ------ | ------ | ------ |
|IPAddressService|00000000-cffb-4231-ad87-db62e82a8017| / |
|IPAddressCharacteristic|00000001-cffb-4231-ad87-db62e82a8017| read string |
|IPAddressDescriptor|00000002-cffb-4231-ad87-db62e82a8017| read string |
|WiFiSSIDCharacteristic|00000003-cffb-4231-ad87-db62e82a8017| read + write string |
|WiFiSSIDDescriptor|00000004-cffb-4231-ad87-db62e82a8017| read string |
|WiFiPWCharacteristic|00000005-cffb-4231-ad87-db62e82a8017| write string |
|WiFiPWDescriptor|00000006-cffb-4231-ad87-db62e82a8017| read string |

## Manual start
**Not required when running as service!**
`python3 bt2wifi.py`
Optional: Set the visible BT name to "BTNAME" (else defaults to "BT2WIFI_DEVICE") via `python3 main.py -name BTNAME`.

## Add as service
1. `cd /lib/systemd/system/`
2. `sudo nano bt2wifi.service`
3. Add following information to the file:
```
[Unit]
Description=BT2WiFi Service
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=lamp
WorkingDirectory=/home/lamp/bt2wifi
ExecStart=/usr/bin/python /home/lamp/bt2wifi/main.py -name BT2WIFI_DEVICE
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```
4. Save and exit file (ctrl + x -> shift + y -> enter)
5. `sudo chmod 644 /lib/systemd/system/bt2wifi.service`
6. `chmod +x /home/lamp/bt2wifi/main.py`
7. `sudo systemctl daemon-reload`
8. `sudo systemctl enable bt2wifi.service`
9. `sudo systemctl start bt2wifi.service`

You can check the status via:
`sudo systemctl status bt2wifi.service`
and the output via:
`sudo journalctl -f -u bt2wifi.service`

After changes to the code you need to refresh the service:
1. `sudo systemctl stop bt2wifi.service`
2. `sudo systemctl daemon-reload`
3. `sudo systemctl start bt2wifi.service`

# How to cite this material
DOI: 10.5281/zenodo.10868729
